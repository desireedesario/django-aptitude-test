from __future__ import unicode_literals
from django.contrib import auth
from django.db import models

class Customer(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	email = models.TextField()
	phone_number = models.IntegerField()

def get_absolute_url(self):
   return reverse('comment_detail', args=[str(self.id)])
