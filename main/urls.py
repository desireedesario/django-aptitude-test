from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^about/$', views.about, name="about"),
    url(r'^thanks/$', views.thanks, name="thanks"),
    url(r'^contact/$', views.ContactView, name="contact"),
    url(r'^customer_list/$', views.CustomerList, name="customer_list"),
    url(r'^(?P<pk>[0-9]+)/$', views.CustomerDetail, name='customer_detail'),
    url(r'^create/$', views.CustomerCreate, name='customer_create'),
    url(r'^(?P<pk>[0-9]+)/update/$', views.CustomerUpdate, name='customer_edit'),
    url(r'^(?P<pk>[0-9]+)/delete/$', views.CustomerDelete, name='customer_delete'),
]
