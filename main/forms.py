from django import forms
from django.core.mail import send_mail


class ContactForm(forms.Form):
    subject = forms.CharField(max_length=75)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()

    def send_email(self):
        # Write some code for sending an email
        #note that email is directed to console at this moment
        send_mail(self['subject'].value(), self['message'].value(), self['sender'].value(), ['desireedesario@gmail.com'], fail_silently=False)
        return super(ContactForm, self)
