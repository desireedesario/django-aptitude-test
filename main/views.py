from django.core.mail import send_mail
from django.shortcuts import render
from django.views.generic.edit import FormView
from django.views.generic import ListView, DetailView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from main.models import Customer
from main.forms import ContactForm

def index(request):
    return render(request, "main/index.html")

def about(request):
    return render(request, "main/about.html")

def thanks(request):
    return render(request, "main/thanks.html")

class ContactView(FormView):
    template_name = "main/contact.html"
    form_class = ContactForm
    success_url = '/thanks/'

    def form_valid(self, form):
        # Write code for after form validation
        ContactForm.send_email(form)
        return super(ContactView, self).form_valid(form)

class CustomerList(ListView):
    model = Customer

class CustomerDetail(DetailView):
    model = Customer

class CustomerCreate(CreateView):
    model = Customer
    fields = ['first_name', 'last_name', 'email', 'phone_number']
    success_url = '/customer_list/'

class CustomerUpdate(UpdateView):
    model = Customer
    fields = ['first_name', 'last_name', 'email', 'phone_number']
    success_url = '/customer_list/'

class CustomerDelete(DeleteView):
    model = Customer
    success_url = '/customer_list/'
