from django.conf.urls import include, url
from django.contrib import admin
from main.views import ContactView
from main.views import CustomerList
from main.views import CustomerDetail
from main.views import CustomerDelete
from main.views import CustomerCreate
from main.views import CustomerUpdate
import sup_world.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^contact/', ContactView.as_view()),
    url(r'^customer_list/$', CustomerList.as_view()),
    url(r'^create/$', CustomerCreate.as_view()),
    url(r'^(?P<pk>[0-9]+)/update/$', CustomerUpdate.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', CustomerDetail.as_view()),
    url(r'^(?P<pk>[0-9]+)/delete/$', CustomerDelete.as_view()),
    url(r'^', include("main.urls", namespace="main", app_name="main")),
]
